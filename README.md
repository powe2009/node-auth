What:
    CRUD application which can be rubberstamped to just about any line of business

Assumptions:
    You already have latest and greatest Node on you pc, 
    MongoDB instance or access to the Clould MongoDB version.

How to install the App:
    from  your app root dir  as in MINGW64 /c/ws/node/node-auth (master)
    run     npm i

How to run:
    from  your app root dir as in dir MINGW64 /c/ws/node/node-auth (master)
    run     npm start

How to see:
    Browser:  localhost:3000
