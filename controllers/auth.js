const bcrypt = require('bcryptjs');

const User = require('../models/user');

exports.getLogin = (req, res, next) => {
  res.render('auth/login', {
    path: '/login',
    pageTitle: 'Login',
    isAuthenticated: false
  });
};

exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  
  User.findOne({email: email}) 
  
    .then(user => {
      if (user) {
      
        bcrypt.compare(password, user.password)
          .then( (isMatch) => {
            req.session.user = user;
            req.session.isLoggedIn = true;
            req.session.save(err => {
              if (!err) {
                return res.redirect('/');
              } else {
                console.log("TCL: exports.postLogin -> err", err);
                return res.redirect('/login');
              }
            })
          })
      } else {
        console.log("TCL: exports.postLogin -> user not found: ", email);
        return res.redirect('/login');
      }
    })
    .catch (err => {
      console.log("TCL: exports.postLogin -> err", err);
      return res.redirect('/login');
    })

};

exports.getSignup = (req, res, next) => {
  res.render('auth/signup', {
    path: '/signup',
    pageTitle: 'Signup',
    isAuthenticated: false
  });
};

exports.postSignup = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;

  User.findOne({ email: email })
    .then(userDoc => {
      if (userDoc) {
        return res.redirect('/signup');
      }
      return bcrypt.hash(password, 12)
        .then( hashedPassword => {
          const user = new User({
            email: email,
            password: hashedPassword,
            cart: { items: [] }  
          });
          return user.save();
      })
      .then (result => {
        res.redirect('/login');
      })
    }); 
};

exports.postLogout = (req, res, next) => {
  req.session.destroy(err => {
    console.log(err);
    res.redirect('/');
  });
};
